<?php require 'views/header.php'; ?>
<main>

    <h1>Edición de productos</h1>

    <form method="post" action="../update/<?php echo $product->id; ?>">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $product->nombre ?>">
        <br>

        <label>Precio</label>
        <input type="text" name="precio" value="<?php echo $product->precio ?>">
        <br>

        <label>Fecha</label>
        <input type="text" name="fecha" value="<?php echo $product->fecha ?>">
        <br>

        <label>Id_tipo</label>
        <input type="text" name="id_tipo" value="<?php echo $product->id_tipo ?>">
        <br>

        <input type="submit" value="Guardar cambios">
        <br>

    </form>
</main>
<?php require 'views/footer.php'; ?>
