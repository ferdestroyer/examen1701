<?php require 'views/header.php'; ?>
<main>

        <h1>Lista de productos</h1>

        <table>
            <tr>
                <th>id</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Fecha</th>
                <th>id_tipo</th>
                <th>Acciones</th>
            </tr>
            <?php foreach ($products as $product): ?>
            <tr>
                <td><?php echo $product->id ?></td>
                <td><?php echo $product->nombre ?></td>
                <td><?php echo $product->precio ?></td>
                <td><?php echo $product->fecha ?></td>
                <td><?php echo $product->id_tipo ?></td>

                <td>
                    <a href="/product/edit/<?php echo $product->id ?>">Editar</a>
                    <a href="/product/visit/<?php echo $product->id ?>">Visitar</a>
                </td>
            </tr>
            <?php endforeach ?>
        </table>


        <p>
            <a href="/product/create">nuevo</a>
        </p>



</main>
<?php require 'views/footer.php'; ?>
