<?php

require_once('models/Product.php');

class ProductController
{

    function __construct()
    {
        //echo "En ProductController";
    }


    //lista de products
    public function index()
    {
        $products = Product::all();
        require("views/products/index.php");
    }

    public function delete($id)
    {
        die('borre el Producto ' . $id);
    }

    public function create()
    {
        $products = Product::all();
        require("views/products/create.php");
    }

    public function store()
    {
        $product = new Product();
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = DateTime::createFromFormat('d-m-Y', $_POST['fecha']);
        $product->store();
        $product->id_tipo = $_POST['id_tipo'];

        $product->store();
        header('location: index');
    }

    //Mostrar productos con datos previos
     public function edit($id)
    {
        $product = Product::find($id);
        include 'views/products/edit.php';
    }

    public function update($id)
    {
        $product = Product::find($id);
        //actualizar campos
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = $_POST['fecha'];
        $product->id_tipo = $_POST['id_tipo'];

        $product->save();
        header('location:../index');
    }

    public function visit($id)
    {
        $product = Product::find($id);
        $_SESSION['product'] = $product;
        header('Location: /product');
    }

}
