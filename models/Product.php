<?php

/**
*
*/
require('app/Model.php');

class Product extends Model
{
    public $id;

    function __construct()
    {
        # code...
    }


    public static function all()
    {
        $db = Product::connect();

        $stmt = $db->query('SELECT * FROM producto');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');
        $results = $stmt->fetchAll();
        return $results;
    }

    //hace el INSERT
    public function store()
    {
        $db = $this->connect();

        $sql = "INSERT INTO producto(nombre, precio, fecha, id_tipo) VALUES(?, ?, ?, ?)";

        $query = $db->prepare($sql);
        $query->bindParam(1, $this->nombre);
        $query->bindParam(2, $this->precio);
        $query->bindParam(3, $this->fecha->format('d-m-Y'));
        $query->bindParam(4, $this->id_tipo);

        return $query->execute();
    }

    public function find($id)
    {
        $db = Product::connect();
        $sql = "SELECT * FROM producto WHERE id=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');
        $result = $stmt->fetch();
        return $result;
    }

    public function save()
    {
        $db = $this->connect();
        $sql = "UPDATE producto SET nombre=?, precio=?, fecha=?, id_tipo=? WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->nombre);
        $stmt->bindParam(2, $this->precio);
        $stmt->bindParam(3, $this->fecha);
        $stmt->bindParam(4, $this->id_tipo);
        $stmt->bindParam(5, $this->id);
        $result = $stmt->execute();
        return $result;
    }

    public function delete()
    {

    }
}
